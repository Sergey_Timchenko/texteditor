const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const autoprefixer = require('autoprefixer');

module.exports = {
    entry: ['webpack/hot/dev-server', './app/index.tsx'],
    output: {
        path: path.join(__dirname, './dist'),
        filename: 'bundle.min.js',
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.jsx', '.css', '.scss'],
        alias: {
            '@types': path.resolve(__dirname, 'app/common/types/'),
            '@components': path.resolve(__dirname, 'app/components/'),
            '@containers': path.resolve(__dirname, 'app/containers/'),
            '@styles-variables': path.resolve(__dirname, 'app/common/styles/variables'),
            '@services': path.resolve(__dirname, 'app/common/services/'),
            '@stores': path.resolve(__dirname, 'app/common/stores/'),
            '@constants': path.resolve(__dirname, 'app/common/constants/'),
        },
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loader: 'awesome-typescript-loader',
            },
            {
                test: /\.scss$/,
                use: [
                    { loader: 'style-loader' },
                    { loader: 'css-loader' },
                    {
                        loader: 'postcss-loader',
                        options: {
                            plugins: [
                                autoprefixer({
                                    overrideBrowserslist:['ie >= 8', 'last 8 version'],
                                }),
                            ],
                        },
                    },
                    { loader: 'sass-loader' },
                ],
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                use: ['file-loader'],
            },
        ],
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './app/index.html',
        }),
    ],
    devServer: {
        historyApiFallback: true,
        contentBase: path.resolve(__dirname, './app/'),
        inline: true,
        hot: true,
    },
};
