import * as React from 'react';
import * as cn from 'classnames';
import {inject, observer} from 'mobx-react';

import './style.scss';
import {EDITOR_STORE_NAME} from '@constants';
import {IEditorService, EditorService} from '@services';
import {IEditorStore} from '@stores';

interface IProps {
    [EDITOR_STORE_NAME: string]: IEditorStore;
}

@inject(EDITOR_STORE_NAME)
@observer
export class EditorView extends React.Component<IProps> {
    private readonly frameRef: React.RefObject<HTMLIFrameElement>;
    private readonly editorService: IEditorService;

    constructor(props: IProps) {
        super(props);

        this.frameRef = React.createRef();
        this.editorService = EditorService.getInstance();
    }

    triggerMousedownEvent = (): void => {
        document.dispatchEvent(new Event('mousedown'));
    };

    componentWillUnmount(): void {
        this.frameRef.current.contentDocument.removeEventListener('mousedown', this.triggerMousedownEvent);
    }

    componentDidMount(): void {
        const {current} = this.frameRef;

        this.editorService.setEditorFrame(current);
        this.props[EDITOR_STORE_NAME].setEditorLoaded(true);
        this.frameRef.current.contentDocument.addEventListener('mousedown', this.triggerMousedownEvent);
    }

    render(): React.ReactNode {
        return (
            <div className={cn('editor-view')}>
                <iframe
                    className={cn('editor-view__frame')}
                    ref={this.frameRef}
                />
            </div>
        );
    }
}
