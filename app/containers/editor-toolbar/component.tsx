import * as React from 'react';
import {inject, observer} from 'mobx-react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

import {Button, ColorPicker, Toolbar, SynonymsList} from '@components';
import {EditorCommands, Tools} from '@types';
import {EditorService, IEditorService} from '@services';
import {DEFAULT_COLOR, EDITOR_STORE_NAME} from '@constants';
import {IEditorStore} from '@stores';
import {ITool, tools} from './tools';

interface IProps {
    [EDITOR_STORE_NAME: string]: IEditorStore;
}

interface IState {
    lastSelectedCommand: EditorCommands;
}

@inject(EDITOR_STORE_NAME)
@observer
export class EditorToolbar extends React.Component<IProps, IState> {
    private readonly editorService: IEditorService;

    constructor(props: IProps) {
        super(props);

        this.editorService = EditorService.getInstance();
        this.state = {lastSelectedCommand: null};
    }

    onToolClick = (command: EditorCommands, argument: string = ''): void => {
        this.editorService.executeCommand(command, argument);
        this.setState({lastSelectedCommand: command});
    };

    getTools = (tools: ITool[]): React.ReactNode[] => {
        return tools.map(({title, type, command, icon}) => {
            if (type === Tools.Button) {
                return (
                    <Button
                        key={command}
                        onClick={() => this.onToolClick(command)}
                        active={this.editorService.isCommandSelected(command)}
                        title={title}
                    >
                        <FontAwesomeIcon icon={icon} />
                    </Button>
                );
            } else if (type === Tools.ColorPicker) {
                return (
                    <ColorPicker
                        key={command}
                        defaultColor={DEFAULT_COLOR}
                        title={title}
                        onColorSelect={(color: string) => this.onToolClick(command, color)}
                    />
                );
            } else if (type === Tools.SynonymsButton) {
                return (
                    <Button
                        key={title}
                        onClick={this.onSynonymsToolClick}
                        title={title}
                    >
                        <FontAwesomeIcon icon={icon} />
                        {
                            this.props[EDITOR_STORE_NAME].showSynonymsList && (
                                <SynonymsList
                                    synonyms={this.props[EDITOR_STORE_NAME].synonyms}
                                    onSynonymSelect={this.onSynonymSelect}
                                    loading={this.props[EDITOR_STORE_NAME].isSynonymsLoading}
                                />
                            )
                        }
                    </Button>

                );
            }

            return null;
        });
    };

    onSynonymSelect = (synonym: string): void => {
        this.props[EDITOR_STORE_NAME].closeSynonymsList();
        this.editorService.setSelectedText(synonym);
    };

    onSynonymsToolClick = ({target}): void => {
        const text = this.editorService.getSelectedText();

        if (text && target.nodeName !== 'DIV') { // hot fix for prevent click on synonym item
            this.props[EDITOR_STORE_NAME].openSynonymsList();
            this.props[EDITOR_STORE_NAME].fetchSynonyms(text);
        }
    };

    render(): React.ReactNode {
        const {isEditorLoaded} = this.props[EDITOR_STORE_NAME];

        return (
            !isEditorLoaded
                ? null
                : (
                    <Toolbar>
                        {this.getTools(tools)}
                    </Toolbar>
                )
        );
    }
}
