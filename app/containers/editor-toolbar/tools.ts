import {EditorCommands, Tools} from '@types';
import {
    faAlignCenter,
    faAlignJustify,
    faAlignLeft,
    faAlignRight,
    faBold,
    faItalic,
    faListOl,
    faListUl,
    faRedo,
    faSubscript,
    faSuperscript,
    faUnderline,
    faUndo,
    faSync,
    IconDefinition,
} from '@fortawesome/free-solid-svg-icons';

export interface ITool {
    type: Tools;
    icon?: IconDefinition;
    command?: EditorCommands;
    title: string;
}

export const tools: ITool[] = [
    {
        icon: faBold,
        command: EditorCommands.Bold,
        type: Tools.Button,
        title: 'Bold',
    },
    {
        icon: faItalic,
        command: EditorCommands.Italic,
        type: Tools.Button,
        title: 'Italic',
    },
    {
        icon: faUnderline,
        command: EditorCommands.Underline,
        type: Tools.Button,
        title: 'Underline',
    },
    {
        icon: faSuperscript,
        command: EditorCommands.Superscript,
        type: Tools.Button,
        title: 'Superscript',
    },
    {
        icon: faSubscript,
        command: EditorCommands.Subscript,
        type: Tools.Button,
        title: 'Subscript',
    },
    {
        command: EditorCommands.ForeColor,
        type: Tools.ColorPicker,
        title: 'Fore color'
    },
    {
        command: EditorCommands.BackColor,
        type: Tools.ColorPicker,
        title: 'Back color'
    },
    {
        icon: faListUl,
        command: EditorCommands.InsertUnorderedList,
        type: Tools.Button,
        title: 'Insert unordered list',
    },
    {
        icon: faListOl,
        command: EditorCommands.InsertOrderedList,
        type: Tools.Button,
        title: 'Insert ordered list',
    },
    {
        icon: faAlignLeft,
        command: EditorCommands.JustifyLeft,
        type: Tools.Button,
        title: 'Justify left',
    },
    {
        icon: faAlignCenter,
        command: EditorCommands.JustifyCenter,
        type: Tools.Button,
        title: 'Justify center',
    },
    {
        icon: faAlignRight,
        command: EditorCommands.JustifyRight,
        type: Tools.Button,
        title: 'Justify right',
    },
    {
        icon: faAlignJustify,
        command: EditorCommands.JustifyFull,
        type: Tools.Button,
        title: 'Justify full',
    },
    {
        icon: faUndo,
        command: EditorCommands.Undo,
        type: Tools.Button,
        title: 'Undo',
    },
    {
        icon: faRedo,
        command: EditorCommands.Redo,
        type: Tools.Button,
        title: 'Redo',
    },
    {
        icon: faSync,
        type: Tools.SynonymsButton,
        title: 'Fetch synonyms',
    },
];
