import * as React from 'react';
import * as cn from 'classnames';

import {EditorToolbar} from '../editor-toolbar';
import {EditorView} from '../editor-view';

import './style.scss';

interface IProps {

}

export class Editor extends React.PureComponent<IProps> {
    render(): React.ReactNode {
        return (
            <div className={cn('editor')}>
                <EditorToolbar />
                <EditorView />
            </div>
        );
    }
}
