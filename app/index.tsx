import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {Provider} from 'mobx-react';

import './common/styles/main.scss';
import {Editor} from '@containers';
import {IEditorStore, EditorStore} from '@stores';
import {EDITOR_STORE_NAME} from '@constants';

const rootElementSelector: string = '#app-root';
const rootElement: HTMLDivElement = document.querySelector(rootElementSelector);
const editorStore: IEditorStore = new EditorStore();
const stores = {
    [EDITOR_STORE_NAME]: editorStore,
};

ReactDOM.render(
    <Provider {...stores}>
        <Editor />
    </Provider>,
    rootElement,
);
