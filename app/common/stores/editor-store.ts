import {observable, action} from 'mobx';
import axios from 'axios';

import {API_URL, SYNONYMS_PARAM_NAME} from '@constants';

export interface IEditorStore {
    isEditorLoaded: boolean;
    isSynonymsLoading: boolean;
    showSynonymsList: boolean;
    synonyms: string[];
    setEditorLoaded: (loaded: boolean) => void;
    fetchSynonyms: (word: string) => void;
    closeSynonymsList: () => void;
    openSynonymsList: () => void;
}

export class EditorStore implements IEditorStore {
    @observable isEditorLoaded: boolean = false;
    @observable isSynonymsLoading: boolean = false;
    @observable showSynonymsList: boolean = false;
    @observable synonyms: string[] = [];

    @action
    setEditorLoaded(loaded: boolean): void {
        this.isEditorLoaded = loaded;
    }

    @action
    fetchSynonyms(word: string): void {
        this.isSynonymsLoading = true;

        axios.get(API_URL, {
            params: {
                [SYNONYMS_PARAM_NAME]: word,
            }
        })
            .then(({data = []}) => {
                this.isSynonymsLoading = false;
                this.synonyms = data.map((item: {word: string}) => item.word);
            })
            .catch((error) => {
                console.error(error);
            });
    }

    @action
    clearSynonyms(): void {
        this.synonyms = [];
    }

    @action
    closeSynonymsList(): void {
        this.showSynonymsList = false;
    }

    @action
    openSynonymsList(): void {
        this.showSynonymsList = true;
    }
}
