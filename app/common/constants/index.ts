export const EDITOR_STORE_NAME: string = 'editorStore';
export const DEFAULT_COLOR: string = '#000000';
export const DEFAULT_COLORS: string[] = [
    '#B80000',
    '#DB3E00',
    '#FCCB00',
    '#008B02',
    '#006B76',
    '#1273DE',
    '#004DCF',
    '#5300EB',
    '#EB9694',
    '#FAD0C3',
    '#FEF3BD',
    '#C1E1C5',
    '#BEDADC',
    '#C4DEF6',
    '#BED3F3',
    '#D4C4FB',
    '#FFFFFF',
    DEFAULT_COLOR,
];
export const API_URL: string = 'http://api.datamuse.com/words/';
export const SYNONYMS_PARAM_NAME: string = 'rel_syn';
