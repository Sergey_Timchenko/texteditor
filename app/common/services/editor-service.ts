import {EditorCommands, DesignMode} from '@types';

export interface IEditorService {
    setEditorFrame: (iframe: HTMLIFrameElement) => void;
    executeCommand: (command: EditorCommands, argument?: string) => boolean;
    isCommandSelected: (command: EditorCommands) => boolean;
    getSelectedText: () => string;
    setSelectedText: (text: string) => void;
}

export class EditorService implements IEditorService {
    private static instance: EditorService;
    private editorFrame: HTMLIFrameElement;
    private editor: Document;

    private constructor() {}

    static getInstance() {
        if (!EditorService.instance) {
            EditorService.instance = new EditorService();
        }

        return EditorService.instance;
    }

    setEditorFrame(iframe: HTMLIFrameElement): void {
        this.editorFrame = iframe;
        this.editor = this.editorFrame.contentDocument;
        this.editor.designMode = DesignMode.On;
    }

    executeCommand(command: EditorCommands, argument: string = ''): boolean {
        if (!this.editorFrame) {
            throw new Error('Editor element is not defined');
        }

        return this.editor.execCommand(command, false, argument);
    }

    isCommandSelected(command: EditorCommands): boolean {
        return this.editor.queryCommandState(command);
    }

    getSelectedText(): string {
        return this.editor.getSelection().toString();
    }

    setSelectedText(text: string): void {
        const textNode = this.editor.createTextNode(text);
        const range = this.editor.getSelection().getRangeAt(0);

        range.deleteContents();
        range.insertNode(textNode);
    }
}
