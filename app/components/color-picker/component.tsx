import * as React from 'react';
import {GithubPicker, ColorResult} from 'react-color';
import * as cn from 'classnames';

import './style.scss';
import {Button} from '@components';
import {DEFAULT_COLORS} from '@constants';

interface IProps {
    children?: React.ReactNode;
    className?: string;
    defaultColor: string;
    title: string;
    onColorSelect: (color: string) => void;
}

interface IState {
    color: string;
    showColorPicker: boolean;
}

export class ColorPicker extends React.Component<IProps, IState> {
    private readonly wrapperRef: React.RefObject<HTMLDivElement>;

    constructor(props: IProps) {
        super(props);

        this.state = {color: props.defaultColor, showColorPicker: false};
        this.wrapperRef = React.createRef();
    }

    componentDidMount(): void {
        document.addEventListener('mousedown', this.handleClickOutside);
    }

    componentWillUnmount(): void {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }

    handleClickOutside = (e): void => {
        if (this.wrapperRef && !this.wrapperRef.current.contains(e.target)) {
            this.setState({showColorPicker: false});
        }
    };

    onColorChange = ({hex}: ColorResult): void => {
        this.props.onColorSelect(hex);
        this.setState({color: hex});
    };

    onToggleColorPicker = (): void => {
        this.setState(({showColorPicker}) => ({showColorPicker: !showColorPicker}));
    };

    render(): React.ReactNode {
        const {className = '', title} = this.props;
        const {color, showColorPicker} = this.state;

        return (
            <div className={cn('color-picker', className)} ref={this.wrapperRef} title={title}>
                <Button onClick={this.onToggleColorPicker}>
                    <div className={cn('color-picker__color')} style={{background: color}} />
                </Button>
                {
                    !showColorPicker
                        ? null
                        : (
                            <div className={cn('color-picker__popover')}>
                                <GithubPicker
                                    color={color}
                                    onChange={this.onColorChange}
                                    colors={DEFAULT_COLORS}
                                />
                            </div>
                        )
                }
            </div>
        );
    }
}
