import * as React from 'react';
import * as cn from 'classnames';

import './style.scss';

interface IProps {
    children?: React.ReactNode;
    className?: string;
}

export const Toolbar: React.FunctionComponent<IProps> = React.memo((props: IProps) => {
    const {children = null, className = ''} = props;

    return (
        <div className={cn('toolbar', className)}>
            {children}
        </div>
    );
});
