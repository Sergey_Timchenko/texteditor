import * as React from 'react';
import * as cn from 'classnames';

import './style.scss';

interface IProps {
    className?: string;
    synonyms: string[];
    loading: boolean;
    onSynonymSelect: (synonym) => void;
}

export const SynonymsList: React.FunctionComponent<IProps> = React.memo((props: IProps) => {
    const {className = '', synonyms = [], onSynonymSelect, loading} = props;

    return (
        <div className={cn('synonyms-list', className)}>
            {
                loading
                    ? <div>Loading</div>
                    : !synonyms.length
                        ? <div>Nothing</div>
                        : (
                            synonyms.map((synonym) => {
                                return (
                                    <div
                                        className={cn('synonyms-list__item')}
                                        key={synonym}
                                        onClick={() => onSynonymSelect(synonym)}
                                    >
                                        {synonym}
                                    </div>
                                );
                            })
                        )
            }
        </div>
    );
});
