import * as React from 'react';
import * as cn from 'classnames';

import './style.scss';

interface IProps extends React.ButtonHTMLAttributes<HTMLButtonElement> {
    active?: boolean;
    children?: React.ReactNode;
    className?: string;
}

export const Button: React.FunctionComponent<IProps> = React.memo((props: IProps) => {
    const {children = null, className = '', active = false, ...restProps} = props;

    return (
        <button
            className={cn('button', className, {'button--active': active})}
            {...restProps}
        >
            {children}
        </button>
    );
});
